from example.new_example import fib, fib_cache


def test_fib():
    res_1 = fib(0)
    res_2 = fib(1)
    res_3 = fib(2)
    res_4 = fib(10)
    assert res_1 == 0
    assert res_2 == 1
    assert res_3 == 1
    assert res_4 == 55


def test_fib_cache():
    res_1 = fib(0)
    res_2 = fib(1)
    res_3 = fib(2)
    res_4 = fib_cache(10)
    assert res_1 == 0
    assert res_2 == 1
    assert res_3 == 1
    assert res_4 == 55


def test_silly_test():
    res_fib = fib(10)
    res_fib_cache = fib_cache(10)
    assert res_fib + res_fib_cache == 110
