def fib(n):
    a, b = 0, 1
    for i in range(1, n+1):
        a, b = b, a + b
    return a


cache = {}


def fib_cache(num):
    if num in cache:
        return cache[num]

    if num == 0:
        result = 0
    elif num == 1 or num == 2:
        result = 1
    else:
        result = fib_cache(num - 2) + fib_cache(num - 1)

    cache[num] = result
    return result


def fib_recursion(num):
    if num == 0:
        return 0
    elif num == 1 or num == 2:
        return 1
    else:
        return fib_recursion(num-2) + fib_recursion(num-1)
